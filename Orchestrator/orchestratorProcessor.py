import datetime
import http.client
import json
import pdb
import os
import sys
import subprocess
import win32ts
#Глобальная переменная - все глобальные значения программы
global mGlobalDict

#Включить WTS (WIN32TS)
mWTSServer = lWTSServer = win32ts.WTSOpenServer("localhost")
#{
#   actionList:
#       [
#           {
#               type: <RemoteMachineProcessingRun>,
#               host: <localhost>,
#               port: <port>,
#               bodyObject: <object dict, int, str, list>
#           },
#           {
#               type: <ActivityLogScheduleListGet>
#           },
#           {
#               type: <ActivityCMDRun>,
#				code: <str>
#           },
#           {
#               type: <ActivityRestartOrchestrator>
#           },
#           {
#               type: <ActivitySessionCheckSetActive>
#           }
#       ]
#
#}

def ProcessingRun(inConfigurationDict):
    #print(mGlobalDict)
    lDateTimeString=datetime.datetime.strftime(datetime.datetime.now(),"%Y.%m.%d %H:%M:%S::%f")
    lResult={"dateTime":lDateTimeString, "state":"connected", "actionListResult":[]}

    for lItem in inConfigurationDict["actionList"]:
        #Добавить входные значения
        lResult["actionListResult"].append({"inArgs":lItem})
        #Обработка запроса на отправку команды на удаленную машину
        if lItem["type"]=="RemoteMachineProcessingRun":
            lHTTPConnection = http.client.HTTPConnection(lItem["host"], lItem["port"], timeout=5)
            try:
                lHTTPConnection.request("POST","/ProcessingRun",json.dumps(lItem["bodyObject"]))
            except Exception as e:
                #Объединение словарей
                lResult["actionListResult"][-1] = {**lResult["actionListResult"][-1], **{"state":"disconnected","errorMessage":str(e)}}
                #lResult["actionListResult"][-1].join({"state":"disconnected","errorMessage":str(e)})
            else:
                lHTTPResponse=lHTTPConnection.getresponse()
                lHTTPResponseByteArray=lHTTPResponse.read()
                lResult["actionListResult"][-1] = {**lResult["actionListResult"][-1], **json.loads(lHTTPResponseByteArray.decode('utf8'))}
        #Обработка команды ActivityLogScheduleListGet
        if lItem["type"]=="ActivityLogScheduleListGet":
            #pdb.set_trace()
            lResult["actionListResult"][-1] = {**lResult["actionListResult"][-1], **{"result":mGlobalDict["ActivityLogScheduleList"]}}
        #Обработка команды PlanLogListGet
        if lItem["type"]=="PlanLogListGet":
            #pdb.set_trace()
            lResult["actionListResult"][-1] = {**lResult["actionListResult"][-1], **{"result":mGlobalDict["JSONConfigurationDict"]["activityList"]}}
        #Обработка команды ActivityCMDRun
        if lItem["type"]=="ActivityCMDRun":
            lCMDCode="cmd /c "+lItem["code"]
            subprocess.Popen(lCMDCode)
            lResultCMDRun=1#os.system(lCMDCode)
            lResult["actionListResult"][-1] = {**lResult["actionListResult"][-1], **{"result":str(lResultCMDRun)}}
        #Обработка команды ActivityRestartOrchestrator
        if lItem["type"]=="ActivityRestartOrchestrator":
            os.execl(sys.executable,os.path.abspath(__file__),*sys.argv)
            sys.exit(0)
        #Обработка команды ActivitySessionCheckSetActive
        if lItem["type"]=="ActivitySessionCheckSetActive":
            lResult["actionListResult"][-1] = {**lResult["actionListResult"][-1], **{"result":{"IsActivated":False}}}
            lPID = os.getpid()
            #pdb.set_trace()
            lSessionId = win32ts.ProcessIdToSessionId(lPID)
            lSessionList = win32ts.WTSEnumerateSessions(lWTSServer)
            #При попытке закрыть сервер возникает ошибка
            #win32ts.WTSCloseServer(lWTSServer)
            #Наложить фильтр
            lSessionFiltered = [d for d in lSessionList if d["SessionId"]==lSessionId]
            #Выполнить переход в активное состояние , если State != 0
            if lSessionFiltered[0]["State"] != 0:
                lCMDCodeTSCON = "tscon " + str(lSeessionId) + " /dest:console"
                lCMDCode="cmd /c "+lCMDCodeTSCON
                subprocess.Popen(lCMDCode)
                lResult["actionListResult"][-1] = {**lResult["actionListResult"][-1], **{"result":{"IsActivated":True}}}
            #Выключить соединение
            
    #Вернуть результат
    return lResult
