from http.server import BaseHTTPRequestHandler, HTTPServer
import pywinauto
import json
import subprocess
import time
import zlib
import os
import PIL
from PIL import ImageGrab
from threading import Thread
import orchestratorProcessor

def SaveScreenshot(inFilePath):
    # grab fullscreen
    lScreenshot = ImageGrab.grab()
    # save image file
    lScreenshot.save('screenshot.png')

#Глобальные переменные
global mActivityLogDict

#inGlobalDict
# "JSONConfigurationDict":<JSON>
class RobotDaemonServer(Thread):
    def __init__(self,name,inGlobalDict):
        Thread.__init__(self)
        self.name = name
        global mJSONConfigurationDict
        mJSONConfigurationDict=inGlobalDict["JSONConfigurationDict"]
        #Перенос переменной в orchestratorProcessor
        orchestratorProcessor.mGlobalDict=inGlobalDict
    def run(self):
        inServerAddress="";
        inPort = mJSONConfigurationDict["webServerPort"];
        print('starting server..., port:'+str(inPort)+" inAddress:"+inServerAddress)
        # Server settings
        # Choose port 8080, for port 80, which is normally used for a http server, you need root access
        server_address = (inServerAddress, inPort)
        httpd = HTTPServer(server_address, testHTTPServer_RequestHandler)
        print('running server...')
        httpd.serve_forever() 


# HTTPRequestHandler class
class testHTTPServer_RequestHandler(BaseHTTPRequestHandler):
    #ResponseContentTypeFile
    def Config(self,inJSONConfigurationDict):
        self.mJSONConfigurationDict=inJSONConfigurationDict
    #ResponseContentTypeFile
    def SendResponseContentTypeFile(self,inContentType,inFilePath):
        # Send response status code
        self.send_response(200)
        # Send headers
        self.send_header('Content-type',inContentType)
        self.end_headers()
        lFileObject = open(inFilePath, "rb") 
        # Write content as utf-8 data
        self.wfile.write(lFileObject.read())
        #Закрыть файловый объект
        lFileObject.close()        
    # GET
    def do_GET(self):
        #Мост между файлом и http запросом (новый формат)
        if self.path == "/":
            self.SendResponseContentTypeFile('text/html',"Web\\Index.xhtml")
        #Мост между файлом и http запросом (новый формат)
        if self.path == '/3rdParty/Semantic-UI-CSS-master/semantic.min.css':
            self.SendResponseContentTypeFile('text/css',"..\\Resources\\Web\\Semantic-UI-CSS-master\\semantic.min.css")
        #Мост между файлом и http запросом (новый формат)
        if self.path == '/3rdParty/Semantic-UI-CSS-master/semantic.min.js':
            self.SendResponseContentTypeFile('application/javascript',"..\\Resources\\Web\\Semantic-UI-CSS-master\\semantic.min.js")
        #Мост между файлом и http запросом (новый формат)
        if self.path == '/3rdParty/jQuery/jquery-3.1.1.min.js':
            self.SendResponseContentTypeFile('application/javascript',"..\\Resources\\Web\\jQuery\\jquery-3.1.1.min.js")
        #Мост между файлом и http запросом (новый формат)
        if self.path == '/3rdParty/Google/LatoItalic.css':
            self.SendResponseContentTypeFile('font/css',"..\\Resources\\Web\\Google\\LatoItalic.css")
        #Мост между файлом и http запросом (новый формат)
        if self.path == '/3rdParty/Semantic-UI-CSS-master/themes/default/assets/fonts/icons.woff2':
            self.SendResponseContentTypeFile('font/woff2',"..\\Resources\\Web\\Semantic-UI-CSS-master\\themes\\default\\assets\\fonts\\icons.woff2")
        #Мост между файлом и http запросом (новый формат)
        if self.path == '/favicon.ico':
            self.SendResponseContentTypeFile('image/x-icon',"Web\\favicon.ico")
        #Мост между файлом и http запросом (новый формат)
        if self.path == '/3rdParty/Handlebars/handlebars-v4.1.2.js':
            self.SendResponseContentTypeFile('application/javascript',"..\\Resources\\Web\\Handlebars\\handlebars-v4.1.2.js")
        #Получить скриншот
        if self.path.split("?")[0] == '/GetScreenshot':
            #Сохранить файл на диск
            SaveScreenshot("Screenshot.png")
            self.SendResponseContentTypeFile('image/png',"Screenshot.png")
        #Monitor
        if self.path == '/Monitor/JSONDaemonListGet':
            # Send response status code
            self.send_response(200)
            # Send headers
            self.send_header('Content-type','application/json')
            self.end_headers()
            # Send message back to client
            message = json.dumps(mJSONConfigurationDict)
            # Write content as utf-8 data
            self.wfile.write(bytes(message, "utf8"))
    # POST
    def do_POST(self):
        #Централизованная функция получения запросов/отправки
        if self.path == '/ProcessingRun':
            #ReadRequest
            lInputObject={}
            if self.headers.get('Content-Length') is not None:
                lInputByteArrayLength = int(self.headers.get('Content-Length'))
                lInputByteArray=self.rfile.read(lInputByteArrayLength)
                #Превращение массива байт в объект
                lInputObject=json.loads(lInputByteArray.decode('utf8'))
            # Send response status code
            self.send_response(200)
            # Send headers
            self.send_header('Content-type','application/json')
            self.end_headers()
            # Send message back to client
            message = json.dumps(orchestratorProcessor.ProcessingRun(lInputObject))
            # Write content as utf-8 data
            self.wfile.write(bytes(message, "utf8"))
        return

#print(ChildProcessReadWaitString(p))
