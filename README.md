# OpenRPA
First open source RPA platform for business is released!

# How to run
Studio
Double click to Studio\StudioRun_32.cmd or Studio\StudioRun_64.cmd

# Robot how to debug
Robot\PythonDebug_64.cmd
import Robot
Robot.ActivityRun(
	{
	   ModuleName: <"GUI"|..., str>,
	   ActivityName: <Function or procedure name in module, str>,
	   ArgumentList: [<Argument 1, any type>, ...] - optional,
	   ArgumentDict: {<Argument 1 name, str>:<Argument 1 value, any type>, ...} - optional
	}
)

# Robot example script:
Robot\Examples\GetFolderList\Python_32_Script_Run.cmd

# Python 32 bit
Resources\WPy32-3720\python-3.7.2\python.exe

# Python 64 bit
Resources\WPy64-3720\python-3.7.2.amd64\python.exe

# Module GUI activity List:
############################
Новая версия
############################
Получить список элементов, который удовлетворяет условиям через расширенный движок поиска
[
   {
       "index":<Позиция элемента в родительском объекте>,
       "depth_start" - глубина, с которой начинается поиск (по умолчанию 1)
       "depth_end" - глубина, до которой ведется поиск (по умолчанию 1)
       "class_name" - наименование класса, который требуется искать
       "title" - наименование заголовка
       "rich_text" - наименование rich_text
   }
]
################
#return: List of UI Object
#inElement - Входной элемент - показатель, что не требуется выполнять коннект к процессу
#inFlagRaiseException - Флаг True - выкинуть ошибку в случае обнаружении пустого списка
#old name - PywinautoExtElementsGet
def UIOSelector_Get_UIOList (inSpecificationList,inElement=None,inFlagRaiseException=True):
#################################################################################################
#Get first (in more than one) UIO (UI Object)
#inSpecificationList - UIOSelector
#inElement - Входной элемент - показатель, что не требуется выполнять коннект к процессу
#inFlagRaiseException - Флаг True - выкинуть ошибку в случае обнаружении пустого списка
#old name - PywinautoExtElementGet
def UIOSelector_Get_UIO (inSpecificationList,inElement=None,inFlagRaiseException=True):
#################################################################################################
#Check if UIO exist (Identified by the UIOSelector)
#inSpecificationList - UIOSelector
#old name - -
def UIOSelector_Exist_Bool (inSpecificationList):
#################################################################################################
#Wait for UIO is appear (at least one of them or all at the same time)
#inSpecificationListList - List of the UIOSelector
#inWaitSecs - Время ожидания объекта в секундах
#inFlagWaitAllInMoment - доп. условие - ожидать появление всех UIOSelector одновременно
#return: [0,1,2] - index of UIOSpecification, which is appear
#old name - -
#####Внимание#####
##Функция ожидания появления элементов (тк элементы могут быть недоступны, неизвестно в каком фреймворке каждый из них может появиться)
def UIOSelectorsSecs_WaitAppear_List (inSpecificationListList,inWaitSecs,inFlagWaitAllInMoment=False):
#################################################################################################
#Wait for UIO is Disappear (at least one of them or all at the same time)
#inSpecificationListList - List of the UIOSelector
#inWaitSecs - Время ожидания пропажи объекта в секундах
#inFlagWaitAllInMoment - доп. условие - ожидать пропажу всех UIOSelector одновременно
#return: [0,1,2] - index of UIOSpecification, which is Disappear
#old name - -
#####Внимание#####
##Функция ожидания пропажи элементов (тк элементы могут быть недоступны, неизвестно в каком фреймворке каждый из них может появиться)
def UIOSelectorsSecs_WaitDisappear_List (inSpecificationListList,inWaitSecs,inFlagWaitAllInMoment=False):
#################################################################################################
#Wait for UIO is appear (at least one of them or all at the same time)
#inSpecificationList - UIOSelector
#inWaitSecs - Время ожидания объекта в секундах
#return: Bool - True - UIO is appear
#old name - -
def UIOSelectorSecs_WaitAppear_Bool (inSpecificationList,inWaitSecs):
#################################################################################################
#Wait for UIO is disappear (at least one of them or all at the same time)
#inSpecificationList - UIOSelector
#inWaitSecs - Время ожидания пропажи объекта в секундах
#return: Bool - True - UIO is Disappear
#old name - -
def UIOSelectorSecs_WaitDisappear_Bool (inSpecificationList,inWaitSecs):
#################################################################################################
#Get process bitness (32 or 64)
#inSpecificationList - UIOSelector
#old name - None
#return None (if Process not found), int 32, or int 64
def UIOSelector_Get_BitnessInt (inSpecificationList):
###########################################################################################################
#inElementSpecificationList = UIOSelector (see description on the top of the document)
#result = pywinauto element wrapper instance or None
#old name - AutomationSearchMouseElement
def UIOSelector_SearchChildByMouse_UIO(inElementSpecification):
####################################################################################################
#inElementSpecification - UIOSelector
#old name - AutomationSearchMouseElementHierarchy
def UIOSelector_SearchChildByMouse_UIOTree(inElementSpecification):
####################################################################################################
#Получить список информационных объектов, который удовлетворяет условиям
#inSpecificationList - UIOSelector
#old name - PywinautoExtElementsGetInfo
def UIOSelector_Get_UIOInfoList (inSpecificationList,inElement=None):
####################################################################################################
#Check is the UIO/UIO's by the UIOSelector exist
#inSpecificationList - UIOSelector
#old name - PywinautoExtElementExist
def UIOSelector_IsExist_Bool (inSpecificationList):
####################################################################################################
#Wait for the UIO by the UIOSelector appear
#inSpecificationList - UIOSelector
#result - { }
#old name - PywinautoExtElementWaitAppear
#############
#Внимание! Старая функция (на замену ей пришла UIOSelectorSecs_WaitAppear_Bool)
#############
def UIOSelector_WaitAppear_Dict(inSpecificationList,inTimeout=60):
####################################################################################################
#Try to restore (maximize) window, if it's was minimized
#(особенность uia backend - он не может прицепиться к окну, если оно свернуто)
#inSpecificationList - UIOSelector
#old name - PywinautoExtTryToRestore
def UIOSelector_TryRestore_Dict(inSpecificationList):
####################################################################################################
#Get the list of the UI object activities
#inControlSpecificationArray - UIOSelector
#old name - ElementActionGetList
def UIOSelector_Get_UIOActivityList (inControlSpecificationArray):
####################################################################################################
#Run the activity in UIO (UI Object)
#inControlSpecificationArray - UIOSelector
#inActionName - UIOActivity (name) from Pywinauto
#old name - ElementRunAction
def UIOSelectorUIOActivity_Run_Dict(inControlSpecificationArray,inActionName,inArgumentList=[],inkwArgumentObject={}):
####################################################################################################
#Get the UIO dict of the attributes
#old name - ElementGetInfo
def UIOSelector_Get_UIOInfo(inControlSpecificationArray):
###################################################################################################
#Get list of child UIO's by Parent UIOSelector
#inControlSpecificationArray- UIOSelector
#old name - ElementGetChildElementList
def UIOSelector_GetChildList_UIOList(inControlSpecificationArray=[],inBackend=mDefaultPywinautoBackend):
####################################################################################################
#Подготовить массив для обращения к поиску элемементов
#inControlSpecificationArray - UIOSelector (can be dirty)
#old name 1 - ElementSpecificationArraySearchPrepare
#old name 2 - ElementSpecificationListNormalize
def UIOSelector_SearchUIONormalize_UIOSelector (inControlSpecificationArray):
####################################################################################################
#Подготовить массив для обращения к поиску процесса (отличается от поиска элемента, тк данная функция нужна для нормализации спецификации для подключения к процессу с окнами)
#inControlSpecificationArray - UIOSelector (can be dirty)
#old name 1 - ElementSpecificationArraySearchPrepare
#old name 2 - ElementSpecificationListNormalize
def UIOSelector_SearchProcessNormalize_UIOSelector (inControlSpecificationArray):
###################################################################################################
#inSpecificationArray - UIOSelector
#old name - ElementDrawOutlineNewFocus
def UIOSelector_FocusHighlight(inSpecificationArray)
###################################################################################################
#Highlight the UI object
#old name - ElementDrawOutlineNew
def UIOSelector_Highlight(inSpecificationArray):

#Dependencies
*  Python 3 x32 [psutil, pywinauto, wmi, PIL, keyboard, pyautogui, win32api (pywin32)]
*  Python 3 x64
*  pywinauto (Windows GUI automation)
*  Semantic UI CSS framework
*  JsRender by https://www.jsviews.com (switch to Handlebars)
*  Handlebars

Created by Unicode Labs (Ivan Maslov)